package com.example.vulnerableapplication

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.html.*
import io.ktor.server.netty.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import kotlinx.html.body
import kotlinx.html.img
import org.slf4j.LoggerFactory

val logger = LoggerFactory.getLogger("main")

fun main() {
    logger.info("Starting HTTP server...")
    embeddedServer(Netty, port = 8080) {
        routing {
            get("/") {
                call.respondHtml(HttpStatusCode.OK) {
                    body {
                        img {
                            src = "https://i.imgur.com/LuszLar.jpg"
                        }
                    }
                }
                logger.info("${call.request.userAgent()} - ${call.request.httpMethod.value} ${call.request.uri}")
            }
        }
    }.start(wait = true)
}